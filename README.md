## Frameworks and tools used:
- Java 14
- Maven
- Selenium
- TestNG
- Parallel Testing

# Instructions
1. Modify CHROME_VERSION in webstaurantstore.src.test.org.webstaurant.pages.BaseTest with the current Chrome Version.

2. Run: mvn test -DbrowserName=chrome

## ToDo

1. Add remote testing
2. It is not working for safari(safari stopped working after latest MacOS update)
