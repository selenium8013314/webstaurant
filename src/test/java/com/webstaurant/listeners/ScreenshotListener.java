package com.webstaurant.listeners;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class ScreenshotListener implements ITestListener {

	@Override
	public void onTestFailure(ITestResult result) {
	  String fileName = "testFailure_" + result.getName() + "_" + result.getEndMillis() + ".png"; // Unique filename with .png extension

	  // Get the WebDriver instance
	  WebDriver driver = (WebDriver) result.getTestContext().getAttribute("webDriver");

	  // Check if the driver is an instance of TakesScreenshot
	  if (driver instanceof TakesScreenshot) {
	    TakesScreenshot screenshot = (TakesScreenshot) driver;
	    try {
	      // Capture the screenshot as a byte array
	      byte[] screenshotBytes = screenshot.getScreenshotAs(OutputType.BYTES);

	      File screenshotFile = new File("screenshots/" + fileName);
	      FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes); // Import FileUtils if needed
	      System.out.println("Screenshot captured for failed test: " + screenshotFile.getAbsolutePath());
	    } catch (IOException e) { // Catch specific exception
	      System.out.println("Failed to capture screenshot: " + e.getMessage());
	    }
	  }
	  else {
	    System.out.println("WebDriver instance doesn't support screenshot capture");
	  }
	}

}






