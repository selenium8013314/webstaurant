package com.webstaurant.pages;

import java.time.Duration;
import java.util.List;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;

import com.webstaurant.utils.Utilities;

public class SearchPage extends Page {

	protected WebDriverWait wait;
	private final int DEFAULT_TIMEOUT = 10;
	private final String WRONG_TITLE = "Stainless Work Table - WebstaurantStore";
	private final String NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR = "li.inline-block.leading-4.align-top.rounded-r-md > a";
	private final String PRODUCTS_LOCATOR_BY_CSS_SELECTOR = "#ProductBoxContainer:has(div.add-to-cart > form > div > div > input.btn.btn-cart.btn-small";
	private final String PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR = "div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span";
	private final String ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME = "addToCartButton";
	private final String ADD_TO_CART_BUTTON_LOCATOR_BY_NAME = "addToCartButton";
	private final String VIEW_CART_ALERT_LOCATOR_BY_XPATH = "//h2[@class='notification__heading']";
	private final String ITEM_ADDED_ALERT_LOCATOR_BY_XPATH = "//div[@class='notification__description']";
	private final String VIEW_CART_BUTTON_LOCATOR_BY_CSS_SELECTOR = "a.btn.btn-small.btn-primary";



	public SearchPage(WebDriver driver) {
		super(driver);
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
	}

	public String getTitle(String titleStr){
		try {
			wait.until(ExpectedConditions.titleContains(titleStr));
			return driver.getTitle();
		} catch (NoSuchElementException e){
			return "e";
		}
	}

	public boolean isLastPage() {

		WebElement nextPageButton = null;
		try {
			switch (driver.getClass().getSimpleName()) {
				case "ChromeDriver", "EdgeDriver" -> {
					nextPageButton = driver.findElement(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR));
				}
				case "FirefoxDriver", "SafariDriver" -> {
					nextPageButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR)));
				}
			}

            assert nextPageButton != null;
			if (nextPageButton.getAttribute("href") != null) {
				int pageNumber = Utilities.getPageNumber(nextPageButton.getAttribute("href"));
				if (pageNumber > -1) {
					nextPageButton.click();
					return false;
				}
			}
		} catch (NoSuchElementException e) {
			// Handle the case where the "next page" button is not found
			return true; // Assuming if the button is not found, it's the last page
		}
		return true;
	}

	public ArrayList<String> searchResultByPage() throws NoSuchElementException {

		ArrayList<String> searchResults = new ArrayList<>();
		List<WebElement> productItems = driver.findElements(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR));

		if (!productItems.isEmpty()) {
			for (WebElement element : productItems) {
				searchResults.add(element.getText());
			}
		}
		return searchResults;
	}

	public void goToLastItemsPage(){

		int nextPage = 1;
		WebElement nextPageButton = null;

		while (nextPage > -1) {
			try {

				switch (driver.getClass().getSimpleName()) {
					case "ChromeDriver", "EdgeDriver" -> {
						nextPageButton = driver.findElement(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR));
					}
					case "FirefoxDriver", "SafariDriver" -> {
						nextPageButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR)));
					}
				}

                assert nextPageButton != null;
				if (nextPageButton.getAttribute("href") != null) {
					nextPage = Utilities.getPageNumber(nextPageButton.getAttribute("href"));
					nextPageButton.click();
				}
			} catch (NoSuchElementException e) {
				System.out.println("Reached last page.");
				break;
			}
		}
	}

	public String addLastItemToCart() {

		String itemAdded = "";
		WebElement addToCartButton = null;
		WebElement item = null;

		try {

			switch (driver.getClass().getSimpleName()) {
				case "ChromeDriver", "EdgeDriver" -> {
					List<WebElement> products = driver.findElements(By.cssSelector(PRODUCTS_LOCATOR_BY_CSS_SELECTOR));

					if (products.isEmpty()) {
						return null; // Indicate no products found
					}

					WebElement lastProduct = products.get(products.size() - 1);

					addToCartButton = lastProduct.findElement(By.name(ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME));
					item = lastProduct.findElement(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR));

				}
				case "FirefoxDriver", "SafariDriver" -> {
					List<WebElement> products = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy((By.cssSelector(PRODUCTS_LOCATOR_BY_CSS_SELECTOR))));

					if (products.isEmpty()) {
						return null; // Indicate no products found
					}

					WebElement lastProduct = products.get(products.size() - 1);
					addToCartButton = wait.until(ExpectedConditions.elementToBeClickable(lastProduct.findElement(By.name(ADD_TO_CART_BUTTON_LOCATOR_BY_NAME))));
					item = wait.until(ExpectedConditions.visibilityOf(lastProduct.findElement(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR))));

				}
			}

            assert item != null;
            itemAdded = item.getText();
			addToCartButton.click();
			return itemAdded;

		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}

	public String addFirstItemToCart() {
		String itemAdded = "";
		List<WebElement> addToCartButtons = null;
		List<WebElement> productItems = null;

		try {
            productItems = switch (driver.getClass().getSimpleName()) {
                case "ChromeDriver", "EdgeDriver" -> {
                    addToCartButtons = driver.findElements(By.name(ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME));
					yield driver.findElements(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR));
                }
                case "FirefoxDriver", "SafariDriver" -> {
                    addToCartButtons = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name(ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME)));
                    yield wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR)));
                }
                default -> productItems;
            };

			if (addToCartButtons != null && productItems != null) {
				itemAdded = productItems.get(0).getText();
				addToCartButtons.get(0).click();
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}


	public String viewCartAlert() {
		try {

			WebElement viewCartAlert = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(VIEW_CART_ALERT_LOCATOR_BY_XPATH)));
			return viewCartAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("View cart alert not found.");
			return "";
		}
	}


	public String getItemAddedAlert() {
		try {

			WebElement itemAddedAlert = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ITEM_ADDED_ALERT_LOCATOR_BY_XPATH)));
			return itemAddedAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Item added message not found.");
			return "";
		}
	}

	public void submitViewCartButton() {
		WebElement viewCartButton = null;
		try {

			switch (driver.getClass().getSimpleName()) {
				case "ChromeDriver", "EdgeDriver" -> {
					viewCartButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(VIEW_CART_BUTTON_LOCATOR_BY_CSS_SELECTOR)));
				}
				case "FirefoxDriver", "SafariDriver" -> {
					viewCartButton = driver.findElement(By.cssSelector("a.btn.btn-small.btn-primary"));
				}
			}

            assert viewCartButton != null;
            viewCartButton.click();

			// Validate second click (optional): sometimes the item adding confirmation alert is displayed twice
			// If the title is equal to self.WRONG_TITLE then a second alert is displayed
			if (this.getTitle().startsWith(WRONG_TITLE))
				viewCartButton.click();

		}
		catch (NoSuchElementException e) {
			System.out.println("No View Cart Button found");
		}
	}
}