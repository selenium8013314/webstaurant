package com.webstaurant.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;

import com.webstaurant.utils.Utilities;

public class CartPage extends Page {

	protected WebDriverWait wait;
	private final int DEFAULT_TIMEOUT = 10;

	private final String ITEMS_QTY_LOCATOR_BY_XPATH = "//input[@class='quantityInput input-mini ']";
	private final String EMPTY_CART_BUTTON_LOCATOR_BY_XPATH = "//button[@class='emptyCartButton btn btn-mini btn-ui pull-right']";
	private final String EMPTY_CART_CONFIRM_BUTTON_LOCATOR_BY_CSS_SELECTOR = "button.border-solid:nth-child(1)";
	private final String TXT_EMPTY_CART_LOCATOR_BY_CLASS_NAME = "header-1";


	public CartPage(WebDriver driver) {
		super(driver);
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
	}

	public boolean getItemAdded(String itemDescription) {
		final String ITEM_DESCRIPTION_LINK_LOCATOR_BY_XPATH = "//a[text()='" + itemDescription + "']";

		WebElement itemDescriptionLink = driver.findElement(By.xpath(ITEM_DESCRIPTION_LINK_LOCATOR_BY_XPATH));

		return itemDescriptionLink.getText().equals(itemDescription);
	}

	public int getItemQty() {
		int qty = -1;

		List<WebElement> itemsQty = driver.findElements(By.xpath(ITEMS_QTY_LOCATOR_BY_XPATH));

		int itemsNo = itemsQty.size() - 1;
		WebElement itemQty = itemsQty.get(itemsNo);
		if (Utilities.isInteger(itemQty.getAttribute("value"))){
			qty = Integer.parseInt(itemQty.getAttribute("value"));
		}
		return qty;
	}

	public String getEmptyCartButtonText() {
		WebElement emptyCartButton = driver.findElement(By.xpath(EMPTY_CART_BUTTON_LOCATOR_BY_XPATH));
		return emptyCartButton.getText();
	}
	public void clickEmptyCartButton() {
		try {
			WebElement emptyCartButton = driver.findElement(By.xpath(EMPTY_CART_BUTTON_LOCATOR_BY_XPATH));
			emptyCartButton.click();
		}
		catch (NoSuchElementException e) {
			System.err.println("No Empty Cart Button found!");
		}
	}

	public void confirmEmptyCart() {
		try {
			WebElement emptyCartConfirmButton = driver.findElement(By.cssSelector(EMPTY_CART_CONFIRM_BUTTON_LOCATOR_BY_CSS_SELECTOR));
			emptyCartConfirmButton.click();
		}
		catch (NoSuchElementException e) {
			System.err.println("No Empty Cart Button found!");
		}
	}

	public String emptyCartMessage() {
		String result = "";

		try {
			WebElement txtEmptyCart = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(TXT_EMPTY_CART_LOCATOR_BY_CLASS_NAME)));
			result = txtEmptyCart.getText();
		}
		catch (NoSuchElementException e) {
			System.err.println("'Your cart is empty.' paragraph not found!");
		}
		return result;
	}


	public boolean emptyCartMessageNotFound() {
		try {
			return wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(TXT_EMPTY_CART_LOCATOR_BY_CLASS_NAME)));
		}
		catch (NoSuchElementException e) {
			System.err.println("'Your cart is empty.' paragraph not found!");
		}
		return false;
	}
}