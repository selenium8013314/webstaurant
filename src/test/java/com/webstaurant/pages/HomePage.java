package com.webstaurant.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class HomePage extends Page {

	private final String SEARCH_TEXT_INPUT_LOCATOR_BY_ID = "searchval";
	private final String OPEN_CART_BUTTON_LOCATOR_BY_ID = "cartItemCountSpan";
	public HomePage(WebDriver driver) {
		super(driver);
	}

	public void searchProduct(String search) {

		WebElement searchBar = driver.findElement(By.id(SEARCH_TEXT_INPUT_LOCATOR_BY_ID));
		searchBar.clear();
		searchBar.sendKeys(search);
		searchBar.submit();

	}

	public void openCart() {
		try {

			WebElement cartButton = driver.findElement(By.id(OPEN_CART_BUTTON_LOCATOR_BY_ID));
			
			if (cartButton != null) {
				cartButton.click();
			}
		}
		catch (NoSuchElementException e) {
			System.out.println("No Open Cart Button found");
		}
	}
}
