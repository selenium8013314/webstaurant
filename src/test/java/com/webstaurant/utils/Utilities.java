package com.webstaurant.utils;
public class Utilities {

	/*Get the next page number based on a url*/
	public static int getPageNumber(String href) {

		if (href.isEmpty()) {
			return -1;
		}
		else
		{
			// if txtPage is greater than 99 and smaller than 1,000
			String txtPage = href.length() >= 3 ? href.substring(href.length() - 3) : href;

			/*
				if textPage.length is integer return textPage
				if textPage.length is not integer and is not empty delete the first character
				if textPage.length is not integer and is less or equal to 1 return -1
			*/
			while (!isInteger(txtPage) && !txtPage.isEmpty()){

				if (txtPage.length() > 1)
					txtPage = txtPage.substring(1);
				else
					return -1;
			}

			return Integer.parseInt(txtPage);

		}



	}

	public static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;

		}
	}

}
