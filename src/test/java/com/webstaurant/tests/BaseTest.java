package com.webstaurant.tests;

import java.util.Map;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import org.testng.ITestContext;

public class BaseTest {
  protected WebDriver driver;
  private static final String HOME_PAGE_URL = "https://www.webstaurantstore.com/";

  protected void openUrl(WebDriver driver){
    driver.get(HOME_PAGE_URL);
  }

  protected WebDriver startDriver(ITestContext testContext) {
    String browserName = System.getProperty("browserName", "chrome"); // Default to Chrome

    switch (browserName) {
      case "chrome":
        driver = getChromeDriver();
        break;
      case "chrome_headless":
        driver = getChromeHeadlessDriver();
        break;
      case "edge":
        driver = getEdgeDriver();
        break;
      case "edge_headless":
        driver = getEdgeDriverHeadless();
        break;
      case "firefox":
        driver = getFirefoxDriver();
        break;
      case "firefox_headless":
        driver = getFirefoxDriverHeadless();
        break;
      case "safari":
        driver = getSafariDriver();
        break;

    }

    testContext.setAttribute("webDriver", driver);
    driver.manage().window().maximize();
    driver.manage().deleteAllCookies();

    return driver;
  }


  protected WebDriver getChromeDriver() {
    WebDriverManager.chromedriver().setup();

    ChromeOptions chromeOptions = setChromeDriverOptions();
    driver = new ChromeDriver(chromeOptions);
    return driver;
  }

  protected WebDriver getChromeHeadlessDriver() {
    WebDriverManager.chromedriver().setup();

    ChromeOptions chromeOptions = setChromeDriverOptions();
    chromeOptions.addArguments("headless=new");
    chromeOptions.addArguments("force-device-scale-factor=0.6");

    driver = new ChromeDriver(chromeOptions);
    return driver;
  }

  // Chrome Driver
  protected ChromeOptions setChromeDriverOptions(){
    ChromeOptions chromeOptions = new ChromeOptions();

    chromeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
    chromeOptions.addArguments("disable-infobars");
    chromeOptions.addArguments("disable-dev-shm-usage");
    chromeOptions.addArguments("disable-features=VizDisplayCompositor");
    chromeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");
    chromeOptions.addArguments("no-sandbox");
    chromeOptions.addArguments("disable-popup-blocking");
    chromeOptions.addArguments("enable-javascript");

    chromeOptions.setExperimentalOption("useAutomationExtension", false);
    chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
    chromeOptions.setExperimentalOption("prefs", Map.of(
            "profile.default_content_setting_values.notifications", 2,
            "profile.default_content_setting_values.geolocation", 1,
            "credentials_enable_service", false,
            "profile.password_manager_enabled", false
    ));

    return chromeOptions;
  }

  // Edge Driver
  protected WebDriver getEdgeDriver() {
    WebDriverManager.edgedriver().setup();

    EdgeOptions edgeOptions = setEdgeDriverOptions();
    driver = new EdgeDriver(edgeOptions);
    return driver;
  }

  protected WebDriver getEdgeDriverHeadless() {
    WebDriverManager.edgedriver().setup();

    EdgeOptions edgeOptions = setEdgeDriverOptions();
    edgeOptions.addArguments("headless=new");
    edgeOptions.addArguments("force-device-scale-factor=0.6");

    driver = new EdgeDriver(edgeOptions);
    return driver;
  }

  protected EdgeOptions setEdgeDriverOptions(){
    EdgeOptions edgeOptions = new EdgeOptions();

    edgeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
    edgeOptions.addArguments("disable-infobars");
    edgeOptions.addArguments("disable-dev-shm-usage");
    edgeOptions.addArguments("disable-features=VizDisplayCompositor");
    edgeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");
    edgeOptions.setExperimentalOption("useAutomationExtension", false);
    edgeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

    return edgeOptions;
  }

  // Firefox Driver
  protected WebDriver getFirefoxDriver() {
    WebDriverManager.firefoxdriver().setup();

    FirefoxOptions firefoxOptions = setFirefoxDriverOptions();

    driver = new FirefoxDriver(firefoxOptions);
    return driver;
  }

  protected WebDriver getFirefoxDriverHeadless() {
    WebDriverManager.firefoxdriver().setup();

    FirefoxOptions firefoxOptions = setFirefoxDriverOptions();
    firefoxOptions.addArguments("--headless");
    firefoxOptions.addArguments("force-device-scale-factor=0.6");

    driver = new FirefoxDriver(firefoxOptions);
    return driver;
  }

  protected FirefoxOptions setFirefoxDriverOptions(){
    FirefoxOptions firefoxOptions = new FirefoxOptions();

    firefoxOptions.addArguments("--disable-notifications","--ignore-certificate-errors");
    firefoxOptions.addArguments("--disable-infobars");
    firefoxOptions.addArguments("--enable-site-per-process=false");

    return firefoxOptions;
  }

  // Safari Driver

  protected WebDriver getSafariDriver() {
    SafariOptions safariOptions = setSafariDriverOptions();

    WebDriverManager.safaridriver().setup();
    driver = new SafariDriver(safariOptions);
    return driver;
  }

  protected SafariOptions setSafariDriverOptions(){
    SafariOptions safariOptions = new SafariOptions();

    safariOptions.setAutomaticProfiling(true);
    safariOptions.setAutomaticInspection(true);

// Following Capabilities are deprecated, verify which are the new ones:

//    safariOptions.setCapability("enableRemoteAutomation", true);
//    safariOptions.setCapability("autoAcceptAlert", true); // Accept alerts automatically (adjust as needed)
//    safariOptions.setCapability("cleanSession", true); // Start a new session each time
//    safariOptions.setCapability("browserName", "safari");
//    safariOptions.setCapability("disable-features", "IsolateOrigins,site-per-process");
//      safariOptions.setCapability("safari.options.preferences",
//              "{\"WebKitPreferences\": {\"developerToolsEnabled\": false, \"javaScriptEnabled\": true}}");
//    safariOptions.setCapability("safari.options.preferences",
//            "{'WebKitPreferences': {'developerToolsEnabled': false, 'javaScriptEnabled': true}}");
//
//    safariOptions.setAutomaticInspection(false);

    return safariOptions;
  }

}
